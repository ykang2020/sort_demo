/**
 * 合并两个有序是数组，根据题目条件，关键是想到从后向前遍历nums1
 * @param {number[]} nums1
 * @param {number} m
 * @param {number[]} nums2
 * @param {number} n
 * @return {void} Do not return anything, modify nums1 in-place instead.
 */
var merge = function (nums1, m, nums2, n) {
  // 定义两个指针分别指向两个数组的有元素的末尾
  let i = m - 1;
  let j = n - 1;
  // 从后向前遍历temp数组
  for (let k = m + n - 1; k >= 0; k--) {
    if (nums1[i] >= nums2[j]) {
      nums1[k] = nums1[i--];
      // 有一种情况就是，nums1数组遍历结束了，nums2数组中还有元素
    } else if (nums1[i] < nums2[j] || i < 0) {
      nums1[k] = nums2[j--];
    }
  }
};
