var sortArray = function (nums) {
  for (let i = 0; i < nums.length; i++) {
    let min = i;
    // 已排序区间 [0, i) ，未排序区间 [i , len)
    // 遍历 i+1 之后的元素找到最小元素的索引
    for (let j = i + 1; j < nums.length; j++) {
      if (nums[j] < nums[min]) {
        min = j;
      }
    }
    // i 与 min 交换
    let temp = nums[i];
    nums[i] = nums[min];
    nums[min] = temp;
  }
  return nums;
};
