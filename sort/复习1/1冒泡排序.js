function bubbleSort(nums) {
  console.time("冒泡排序耗时");
  const N = nums.length;

  for (let i = 0; i < N; i++) {
    let flag = true;
    for (let j = 0; j < N - 1 - i; j++) {
      if (nums[j] > nums[j + 1]) {
        swap(nums, j, j + 1);
        flag = false;
      }
    }
    if (flag) {
      break;
    }
  }
  console.timeEnd("冒泡排序耗时");
  return nums;
}

function swap(arr, i, j) {
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

let arr = bubbleSort([6, 9, 0, 4, 8, 3, 2, 7, 1, 5]);
console.log(arr);
