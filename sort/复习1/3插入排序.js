var sortArray = function (nums) {
  console.time("插入排序耗时");
  const N = nums.length;
  // for (let i = 1; i < N; i++) {
  //   let temp = nums[i];
  //   let j = i;
  //   while (j > 0 && nums[j - 1] > temp) {
  //     nums[j] = nums[j - 1];
  //     j--;
  //   }
  //   nums[j] = temp;
  // }
  for (let i = 1; i < N; i++) {
    for (let j = i; j > 0 && nums[j - 1] > nums[j]; j--) {
      swap(nums, j, j - 1);
    }
  }
  console.timeEnd("插入排序耗时");
  return nums;
};

function swap(arr, i, j) {
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

let arr = sortArray([6, 9, 0, 4, 8, 3, 2, 7, 1, 5]);
console.log(arr);
