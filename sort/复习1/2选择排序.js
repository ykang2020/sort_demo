function selectionSort(nums) {
  console.time("选择排序耗时");
  const N = nums.length;
  for (let i = 0; i < N; i++) {
    let minIndex = i;
    for (let j = i; j < N; j++) {
      if (nums[j] < nums[minIndex]) {
        minIndex = j;
      }
    }
    swap(nums, i, minIndex);
  }

  console.timeEnd("选择排序耗时");
  return nums;
};

function swap(arr, i, j) {
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

let arr = selectionSort([6, 9, 0, 4, 8, 3, 2, 7, 1, 5]);
console.log(arr);
