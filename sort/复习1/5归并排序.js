function sortArray(nums) {
  console.time("归并排序耗时");
  const N = nums.length;
  const temp = [];
  mergeSort(nums, 0, N - 1, temp);
  console.timeEnd("归并排序耗时");
  return nums;
}
function mergeSort(nums, left, right, temp) {
  if (left >= right) {
    return;
  }
  let mid = (left + right) >> 1;
  mergeSort(nums, left, mid, temp);
  mergeSort(nums, mid + 1, right, temp);
  merge(nums, left, mid, right, temp);
}

function merge(nums, left, mid, right, temp) {
  for (let k = left; k <= right; k++) {
    temp[k] = nums[k];
  }
  let i = left;
  let j = mid + 1;
  for (let k = left; k <= right; k++) {
    if (temp[i] <= temp[j]) {
      nums[k] = temp[i];
      i++;
    } else if (temp[j] < temp[i]) {
      nums[k] = temp[j];
      j++;
    } else if (i > mid) {
      nums[k] = temp[j];
      j++;
    } else {
      nums[k] = temp[i];
      i++;
    }
  }
}

function swap(arr, i, j) {
  let temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

let arr = sortArray([6, 9, 0, 4, 8, 3, 2, 7, 1, 5]);
console.log(arr);
