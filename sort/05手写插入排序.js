var sortArray = function (nums) {
  for (let i = 1; i < nums.length; i++) {
    // 已排序区间 [0, i) ，未排序区间 [i , len)
    // 将 nums[i] 插入到区间 [0, i) 使之成为有序数组
    for (let j = i; j > 0 && nums[j] < nums[j - 1]; j--) {
      // 将nums[i]与nums[0]到nums[i-1]中比它小的所有元素依次有序地交换
      // j 与 j-1 交换位置 
      let temp = nums[j];
      nums[j] = nums[j - 1];
      nums[j - 1] = temp;
    }
  }
  return nums;
};
