public class Solution {

  public int reversePairs(int[] nums) {
    int len = nums.length;
    if (len < 2) {
      return 0;
    }
    int[] copy = new int[len];
    for (int i = 0; i < len; i++) {
      copy[i] = nums[i];
    }
    return 0;
  }
}